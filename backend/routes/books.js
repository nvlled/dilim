const express = require('express');
const router = express.Router();
const api = require("../api");

router.get('/register', function (req, res, next) {
    let book = req.query;
    console.log("registering book", book);
    const stat = api.registerBook(book)
    res.send(stat);
});

router.get('/update', function (req, res, next) {
    let book = req.query;
    console.log("updating book", book);
    const stat = api.updateBook(book)
    res.send(stat);
});

router.get('/search', function (req, res, next) {
    const books = api.getBooks(req.query);
    res.send(books);
});

module.exports = router;
