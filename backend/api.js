const dbName = "lib.db";
const db = require('better-sqlite3')(dbName);
const process = require("process");

let bookColumns = [
    "id", "classNumber", "classHeading",
    "subject",
    "authorFname", "authorLname", "title",

    "edition", "publisher", "location", "year", "copyright",
    "copyNumber", "cutterNumber", "callNumber",

    "locator", "illustrations", 
    //"hasIndex", "hasGlossary", "hasBibliography",
    "isbn",
    "pagesFront", "pagesContent", "size",
    "partOfBook",
    "remarks",

    "picPath", "softCopyPath",
    "available", "checkoutId",

    "accessionDate",
    "otherInfo",
];

const initDB = () => {
    db.exec(`
        create table if not exists books (
            id integer primary key,
            classNumber varchar(4),
            subject varchar(512),
            classHeading varchar(256),
            authorFname varchar(256),
            authorLname varchar(256),
            title text not null,

            edition integer default 1,
            publisher varchar(512),
            location varchar(512),
            year varchar(5),
            copyright integer,
            copyNumber varchar(10),
            cutterNumber varchar(5),
            callNumber varchar(50),
            remarks varchar(120),

            locator varchar(100),
            illustrations varchar(1024),
            partOfBook varchar(1024),

            --hasIndex tinyint(1) default false,
            --hasGlossary tinyint(1) default false,
            --hasBibliography tinyint(1) default false,

            isbn varchar(20),
            pagesFront string,
            pagesContent integer,
            size varchar(100),

            picPath text,
            softCopyPath text,
            available bool default true,
            checkoutId integer,

            accessionDate date,
            otherInfo varchar(1024)
        );
        create table if not exists borrowedBooks (
            id integer primary key,
            bookId integer,
            borrowerId integer,
            borrowDate date,
            returnDate date
        );
        create table if not exists borrowers (
            id integer primary key,
            firstname varchar(256),
            lastname varchar(256),
            midname varchar(256),
            email varchar(256) unique,
            contactNumber varchar(20)
        );
        create table if not exists librarians (
            id integer primary key,
            username varchar(128) unique,
            password varchar(512),
            firstname varchar(256),
            lastname varchar(256)
        );
    `);
}

const config = {
    defaultPageSize: 15,
}

const util = {
    getSqlLimit(page=1, pageSize=config.defaultPageSize) {
        return {
            limit: pageSize,
            offset: (page-1)*pageSize,
        }
    },

    pluck(obj, keys) {
        const obj_ = {};
        for (let [k, v] of Object.entries(obj)) {
            if (keys.indexOf(k) >= 0)
                obj_[k] = v;
        }
        return obj_;
    },

    makeNameValues(fields) {
        const names = fields.join(", ");
        const values = fields.map(f => `@${f}`).join(", ");
        return { names, values }
    },
}

const api = {
    oneRow(sql, params) {
        try {
            const st = db.prepare(sql);
            return st.get(params);
        } catch (e) {
            return {};
        }
    },

    getBooks({page, pageSize, query="", orderBy="title", type="title", desc=false} = {}) {
        query = `%${query}%`;

        if (!type) {
            type = "title,authorFname,authorLname";
        }
        const where = type.split(",").map(t => `${t.trim()} like @query`).join(" or ");
        const condStr = `where ${where}`;
        const descStr = desc ? "desc" : "asc";

        const countSql = `
            SELECT count(*) as count FROM books ${condStr}
            order by @orderBy ${descStr}
        `;

        const sql = `
            SELECT * FROM books ${condStr}
            order by @orderBy ${descStr}
            limit @limit offset @offset
        `;
        try {
            const {limit, offset} = util.getSqlLimit(page, pageSize);
            const params = {query, limit, offset, orderBy};
            const st = db.prepare(sql);

            const countResult = this.oneRow(countSql, params);
            return {
                count: countResult.count || 0,
                rows: st.all(params),
            }
        } catch (e) {
            return { error: e.message }
        }
    },

    updateBook(book) {
        const fields = bookColumns.filter(name => book[name] !== undefined);

        if (fields.length == 0) {
            return { error: "invalid book data"}
        }

        let falsify = val => ['false', '0'].indexOf(val) >= 0 ? 0 : 1;

        let kvs = fields
            .filter(f => f != "id")
            .filter(f => !f.match(/^has/))
            .map(f => `${f}=@${f}`).join(", ");
        const st = db.prepare(`
            update books set ${kvs}
            where id=@id
            `);

        try {
            let resp = st.run(book);
            return resp;
        } catch (e) {
            console.log(e);
            let error = "failed to register book, " + e.message;
            if (e.message.match(/constraint.*callNumber/i)) {
                error = "call number is already used, try a different one";
            }
            return { error };
        }
    },

    registerBook(book) {
        const fields = bookColumns.filter(name => book[name] !== undefined);

        if (fields.length == 0) {
            return { error: "invalid book data"}
        }

        const { names, values} = util.makeNameValues(fields);
        const st = db.prepare(`insert into books(${names}) values(${values})`);

        try {
            let {lastInsertRowid} = st.run(book);
            return {
                id: lastInsertRowid,
            }
        } catch (e) {
            console.log(e);
            console.log(book.id, book.title);
            let error = "failed to register book, " + e.message;
            if (e.message.match(/constraint.*callNumber/i)) {
                error = "call number is already used, try a different one";
            }
            return { error };
        }
    },

    getBook(id) {
        return db.prepare("select * from books where id=@id").get({id});
    },

    returnBook(bookId) {
        const book = this.getBook(bookId);
        if (!book) {
            return { error: "invalid book id" }
        }
        if (book.checkoutId == null) {
            return { error: "book is not borrowed" }
        }

        db.transaction(() => {
            const fields = [ "bookId", "borrowerId", "borrowDate" ];
            const { names, values} = util.makeNameValues(fields);

            let {checkoutId} = book;
            let st = db.prepare(`
                update borrowedBooks
                set returnDate=@returnDate
                where bookId=@bookId and id=@checkoutId
            `);
            st.run({
                bookId,
                returnDate: Date.now(),
                checkoutId,
            });

            st = db.prepare("update books set checkoutId=null where id=@id");
            st.run({id: bookId });
        })();
    },

    borrowBook(bookId, borrowerId) {
        const book = this.getBook(bookId);
        if (!book) {
            return { error: "invalid book id" }
        }
        if (!book.available) {
            return { error: "book is not currently not available for borrowing" }
        }
        if (book.checkoutId != null) {
            return { error: "book is currently borrowed" }
        }

        db.transaction(() => {
            const fields = [ "bookId", "borrowerId", "borrowDate" ];
            const { names, values} = util.makeNameValues(fields);

            let st = db.prepare(`insert into borrowedBooks(${names}) values(${values})`);
            let {lastInsertRowid: checkoutId} = st.run({
                bookId,
                borrowerId,
                borrowDate: Date.now(),
            });
            console.log("checkoutId", checkoutId);
            st = db.prepare("update books set checkoutId=@checkoutId where id=@id");
            st.run({id: bookId, checkoutId});
        })();
    },

    getBorrowedBooks(borrower) {
    },

    getBorrowers({page, pageSize, query="", orderBy="firstname", type="firstname", desc=false} = {}) {
        query = `%${query}%`;
        const condStr = type ? `where ${type} like @query` : "";
        const descStr = desc ? "desc" : "asc";

        // NOTE!!: ${orderBy} is vulnerable, or error-prone
        // I just put it like that for the mean time because
        // @orderBy isn't working as expected
        const sql = `
            select * from borrowers ${condStr}
            order by ${orderBy} ${descStr}
            limit @limit offset @offset
        `;
        try {
            const st = db.prepare(sql);
            const {limit, offset} = util.getSqlLimit(page, pageSize);

            return {
                rows: st.all({query, limit, offset, orderBy}),
            }
        } catch (e) {
            console.log(e);
            return { error: e.message }
        }
    },

    registerBorrower(borrower) {
        const fields = [
            "firstname", "lastname", "midname",
            "email", "contactNumber",
        ].filter(name => borrower[name] !== undefined);

        if (fields.length == 0) {
            return { error: "invalid borrower data"}
        }

        const {names, values} = util.makeNameValues(fields);
        const st = db.prepare(`insert into borrowers(${names}) values(${values})`);

        try {
            let {lastInsertRowid} = st.run(borrower);
            return {
                id: lastInsertRowid,
            }
        } catch (e) {
            let error = "failed to register borrower, " + e.message;
            return { error };
        }
    },
}

initDB();

function test() {
    console.log(api.registerBook({title: Math.random().toString(36).slice(2, 9), authorFname: "aaaa" }));
    console.log(api.registerBorrower({firstname: Math.random().toString(36).slice(2, 5), lastname: "bbbb"   }));

    let bookRes = api.getBooks({type: "title", query: "", pageSize: 5, page: 1});
    console.log(bookRes.error);
    if (bookRes.rows) {
        for (let book of bookRes.rows) {
            console.log(">", util.pluck(book, ["id", "title", "authorFname", "checkoutId"]));
        }
    }

    let borRes = api.getBorrowers({orderBy: "id", desc: true, type: "firstname", query: "", pageSize: 5, page: 1});
    console.log(borRes.error);
    if (borRes.rows) {
        for (let book of borRes.rows) {
            console.log(">", util.pluck(book, ["id", "firstname"]));
        }
    }

    for (let row of db.prepare("select * from borrowedBooks").all())
        console.log(row);

    console.log(api.borrowBook(2, 2));
    console.log(api.returnBook(2));

}

module.exports = api;
