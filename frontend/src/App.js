import {range, bookUtil, andJoin, commaJoin, surround, prefix, suffix} from "./util";
import {AuthorCard, SubjectCard, TitleCard} from "./Cards";

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import * as ddcsum from "ddcsum";
import * as ordinal from "ordinal";
import html2pdf from "html2pdf.js";
const axios = require("axios");


export const atMost = (millis, fn) => {
    let waiting = false;
    let savedArgs = [];
    return (...args) => {
        savedArgs = args;
        if (waiting) {
            return;
        }
        waiting = true;
        setTimeout(() => {
            fn(...savedArgs);
            waiting = false;
        }, millis);
    }
}


class BrowseBooks extends Component {
    state = {
        books: [],
        isRegister: false,
        editBook: null,
        type: "title",
        query: "",
        page: 1,
        pageSize: 10,
        count: 0,
    }
    bookType = React.createRef();
    componentDidMount() {
    }

    fetch(query, type) {
        if (!query)
            query = this.state.query;
        if (!type)
            type = this.state.type;
        let {page, pageSize} = this.state;
        axios.get('/books/search', {
            params: {
                query,
                page,
                pageSize,
                type,
            },
        }).then(resp => {
            if (!resp.data || resp.data.error) {
                console.log(resp.error);
                return;
            }
            const {rows=[], count=0} = resp.data || {};
            this.setState({ books: rows, count })
        });
    }

    handleShowRegister = e => { this.setState({ register: true }) }
    handleHideRegister = e => { this.setState({ register: false }) }

    handleCancel = e => {
        this.setState({ register: false, editBook: null })
    }

    handleRegister = e => {
        this.setState({ register: false })
        this.fetch();
    }

    handleShowEdit = book => {
        this.setState({ editBook: book });
    }
    handleEdit = book => {
        this.setState({ editBook: null })
        this.fetch();
        if (this.props.onUpdate) {
            this.props.onUpdate(book);
        }
    }

    handleSearch = e => {
        const query = e.target.value;
        const type = this.state.type;
        this.updateSearch(query, type);
    }

    handleTypeChange = e => {
        const query = this.state.query;
        const type = e.target.value;
        this.updateSearch(query, type);
    }

    updateSearch(query, type) {
        if (!query) {
          this.setState({ books: [], count: 0, });
          return;
        }
        this.setState({ query, type }, () => this.fetch());
    }

    setPage = page => {
        this.setState({page}, () => this.fetch());
    }

    render() {
        const {
            books,
            register,
            editBook,
            count,
            pageSize,
        } = this.state;
        const numPages = Math.ceil(count/pageSize);
        return <div className="panel main">
            { (!register && !editBook) && <React.Fragment>
            <div className="browse-books--search">
                <input placeholder={"search term"} onChange={this.handleSearch}/>
                <select ref={this.bookType} onChange={this.handleTypeChange}>
                    <option>title</option>
                    <option value="authorFname,authorLname">author</option>
                    <option>publisher</option>
                    <option>subject</option>
                    <option>year</option>
                </select>
                <br />
                <span>search result count: {count}</span>
            </div>
            {books.length > 0 &&
            <React.Fragment>
            <table className="browse-books--results">
                <thead>
                    <tr>
                        <th style={{width: "10px"}}>accession num.</th>
                        <th>title</th>
                        <th>author</th>
                        <th>year</th>
                        <th>call number</th>
                        <th>ISBN</th>
                        <th>actions</th>
                    </tr>
                </thead>
                <tbody>
                {books.map(book =>
                    <tr>
                        <td>{(book.id+"").padStart(4, "0")}</td>
                        <td>{book.title}</td>
                        <td>{bookUtil.author(book)}</td>
                        <td>{book.year}</td>
                        <td>{book.callNumber}</td>
                        <td>{book.isbn}</td>
                        <td>
                            <a href="#edit" onClick={() => this.handleShowEdit(book)}>edit</a>
                        </td>
                    </tr>
                )}
                </tbody>
            </table>
            <ul className="inline">
                {numPages > 1 && <React.Fragment>
                    {range(Math.floor(numPages)).map(n =>
                        <li><Link text={n+1} onClick={() => this.setPage(n+1)}/></li>
                    )}
                </React.Fragment>}
            </ul>
            </React.Fragment>
            }
            <div class="right">
                <button onClick={this.handleShowRegister}>register new book</button>
            </div>
            </React.Fragment>}
            {register && <RegisterBook onSubmit={this.handleRegister} onCancel={this.handleCancel}/>}
            {editBook &&
                <RegisterBook edit={true}
                    bookData={editBook}
                    onSubmit={this.handleEdit}
                    onCancel={this.handleCancel}/>}
        </div>
    }
}

class CardGrid  extends Component {
    render() {
        const cards = this.props.children || [];
        const {
            cols=2,
            rows=2,
            orientation="portrait",
            format='a4',
        } = this.props;
        const colClass = `col${cols}`;
        const rowClass = `row${rows}`;
        return <div className={`card-grid format ${format} ${orientation} ${colClass} ${rowClass}`} >
            {cards}
        </div>;
    }
}

class RegisterBorrower extends Component {
        bookData =  {
            classNumber: "",
            classHeading: "",
            title: "",
            subject: "",
            firstname: "",
            lastname: "",
            publisher: "",
            publishYear: "",
            cutterNumber: "",
            callNumber: "",
            copyNumber: "",
        }
}

class RegisterBook extends Component {
    state = {
        bookData: {},
    }
    componentDidMount() {
        const {bookData} = this.props;
        if (bookData) {
            this.setState({ bookData });
            this.updateBookData(bookData);
        }
    }

    handleCheck = e => {
        const input = e.target;
        const bookData = {
            ...this.state.bookData,
            [input.name]: input.checked,
        }
        this.setState({
            bookData: bookData,
        });
    }

    handleChange = e => {
        const input = e.target;
        const bookData = {
            ...this.state.bookData,
            [input.name]: input.value,
        }
        this.setState({
            bookData: bookData,
        });
        this.updateBookData(bookData);
    }

    updateBookData = atMost(512, bookData => {
        const {
            classNumber,
            title,
            authorFname="",
            authorLname="",
            year,
            copyNumber,
        } = bookData;

        const classHeading = ddcsum.data[classNumber];
        const cutterNumber = ddcsum.generateCutterNumber(authorLname||"", authorFname||"");
        const callNumber = ddcsum.generateCallNumber({
                classNumber,
                title,
                author: { firstname: authorFname, lastname: authorLname },
                publishYear: year,
                copyNumber,
        });

        this.setState({
            bookData: {
                ...bookData,
                classHeading,
                cutterNumber,
                callNumber,
            }
        });
    });

    registerBook(book) {
        let url = this.props.edit ? '/books/update' : '/books/register';
        axios.get(url, {
            params: book,
        }).then(resp => {
            const { onSubmit } = this.props;
            if (onSubmit) {
                onSubmit(this.state.bookData);
            }
        });
    }

    handleCancel = e => {
        const {onCancel} = this.props;
        if (!onCancel) {
            console.warn("no onCancel handler for RegisterBook");
        } else {
            onCancel(this.state.bookData);
        }
    }

    handleSubmit = e => {
        this.registerBook(this.state.bookData);
    }

    render() {
        const {bookData} = this.state;
        return <div>
            <div>
                <span>Title </span>
                <input name="title" value={bookData.title} onChange={this.handleChange} />
            </div>
            <div>
                <span>Subject </span>
                <input name="subject" value={bookData.subject} onChange={this.handleChange} />
            </div>
            <div>
                <span>Class Number </span>
                <input name="classNumber" value={bookData.classNumber} onChange={this.handleChange} list="summary" />
                <datalist id="summary">
                    {Object.entries(ddcsum.data).map(([classNum, heading]) =>
                        <option value={classNum}>{heading}</option>
                    )}
                </datalist>
                <span> {bookData.classHeading}</span>
            </div>
            <div>
                <span>Author </span>
                <input name="authorFname" value={bookData.authorFname} onChange={this.handleChange} />
                <input name="authorLname" value={bookData.authorLname} onChange={this.handleChange} />
            </div>
            <div>
                <span>Year </span>
                <input name="year" value={bookData.year} onChange={this.handleChange} />
            </div>
            <div>
                <span>Copy Number </span>
                <input name="copyNumber" value={bookData.copyNumber} onChange={this.handleChange} />
            </div>
            <div>
                <span>Cutter Number </span>
                <input name="classNumber" value={bookData.cutterNumber} />
            </div>
            <div>
                <span>Call Number </span>
                <input name="classNumber" value={bookData.callNumber} />
            </div>

            <fieldset>
                <div>
                    <span>ISBN </span>
                    <input name="isbn" value={bookData.isbn} onChange={this.handleChange} />
                </div>
                <div>
                    <span>Edition </span>
                    <input name="edition" value={bookData.edition} type='number' onChange={this.handleChange} />
                </div>
                <div>
                    <span>Publisher Name </span>
                    <input name="publisher" value={bookData.publisher} onChange={this.handleChange} />
                </div>
                <div>
                    <span>Publisher Location </span>
                    <input name="location" value={bookData.location} onChange={this.handleChange} />
                </div>
                <div>
                    <span>Pagination </span>
                    <input name="pagesFront" size='5' placeholder='xxiv' value={bookData.pagesFront} onChange={this.handleChange} />
                    <input name="pagesContent" size='5' placeholder='345' value={bookData.pagesContent} onChange={this.handleChange} />
                    <span>{bookData.pagesFront}{surround(", ", bookData.pagesContent, "p.")}</span>
                </div>
                <div>
                    <span>Size/dimensions </span>
                    <input name="size" value={bookData.size} placeholder='26cm.' onChange={this.handleChange} />
                </div>
                <div>
                    <span>Locator </span>
                    <input name="locator" value={bookData.locator} placeholder='Shelf Location e.g., GC' onChange={this.handleChange} />
                </div>
            </fieldset>

            <fieldset>
                <div>
                    <span>Illustrations </span>
                    <input name="illustrations" value={bookData.illustrations} onChange={this.handleChange} />
                </div>
                <div>
                <span>Parts of Book </span>
                    <input placeholder={'index, bibliography, etc.'} name="partOfBook" value={bookData.partOfBook} onChange={this.handleChange} />
                </div>
                <div>
                    <span>Remarks </span>
                    <input name="remarks" placeholder={'pbk, hb'} value={bookData.remarks} onChange={this.handleChange} />
                </div>
                <div>
                    <span>Other Info</span>
                    <br />
                    <textarea name="otherInfo" rows="7" cols="50" placeholder={'Other info, shows up at the bottom of the card'} value={bookData.otherInfo} onChange={this.handleChange} />
                </div>
            </fieldset>

            <div>
                <button onClick={this.handleSubmit}>submit</button>
                <button onClick={this.handleCancel}>cancel</button>
            </div>
        </div>;
    }
}

class ConfigCard extends Component {
    pageFormats = [
        'a0', 'a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', 'a10',
        'b0', 'b1', 'b2', 'b3', 'b4', 'b5', 'b6', 'b7', 'b8', 'b9', 'b10',
        'c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', 'c10',
        'dl', 'letter', 'government-letter',
        'legal', 'ledger',
        'tabloid', 'credit-card',
    ];

    state = {
        columns: 2,
        rows: 1,
        cardSize: 0,
        orientation: "landscape",
        format: "a4",
    }

    componentDidMount() {
        const props = this.props || {};
        this.setState(state => ({...state, ...props}));
    }

    handleChange = e => {
        const input = e.target;
        const state = {
            ...this.state,
            [input.name]: input.value,
        }
        this.setState(state);
        if (this.props.onChange) {
            this.props.onChange(state);
        } else {
            console.log("card config", state);
        }
    }

    render() {
        const onChange = this.handleChange;
        const {
            rows,
            columns,
            cardSize,
            orientation,
            format,
        } = this.state;
        return <div className="flex-row config-card">
            <div>
            <span>{'Card Columns '}</span>
            <select name="columns" value={columns} onChange={onChange}>
                <option value={1}>1</option>
                <option value={2}>2</option>
                <option value={3}>3</option>
                <option value={4}>4</option>
                <option value={5}>5</option>
            </select>
            </div>

            <div>
            <span>{'Orientation'} </span>
            <select name="orientation" value={orientation} onChange={onChange}>
                <option value='portrait'>portrait</option>
                <option value='landscape'>landscape</option>
            </select>
            </div>

            <div>
            <span>{'Page Format '}</span>
            <select name="format" value={format} onChange={onChange}>
                {this.pageFormats.map(v =>
                    <option value={v}>{v.replace(/-/g, ' ')}</option>
                )}
            </select>
            </div>

            {/*
                <div>
                    {'Card Rows '}
                    <select name="rows" value={rows} onChange={onChange}>
                        <option value={1}>1</option>
                        <option value={2}>2</option>
                        <option value={3}>3</option>
                        <option value={4}>4</option>
                        <option value={5}>5</option>
                    </select>
                </div>
                <div>
                    {'Card size '}
                    <select name="cardSize" value={cardSize} onChange={onChange}>
                        <option value={0}>5x3 inches</option>
                        <option value={1}>3x5 inches</option>
                        <option value={2}>3x3 inches</option>
                        <option value={3}>5x5 inches</option>
                    </select>
                </div>
                */}
        </div>;
    }
}

class SelectBooks extends Component {
    state = {
        inputVal: "",
        matchingBooks: [],
        addedBooks: [],
    }
    componentDidMount() {
        if (this.props.books) {
            this.setState({ addedBooks: this.props.books });
        }
    }
    handleSearch = e => {
        const query = e.target.value.trim();
        this.setState({ inputVal: query});
        if (!query) {
            this.setState({ matchingBooks: [] });
            return;
        }
        axios.get("/books/search?id=huh", {
            params: { type: "title,authorFname,authorLname", query, pageSize: 100 },
        }).then(res => {
            const data = res.data || {};
            this.setState({ matchingBooks: data.rows });
        });
    }
    handleAdd = book => {
        book.key = "bk"+(+new Date);
        const addedBooks = this.state.addedBooks.concat(book)
        this.setState(state => ({
            inputVal: "",
            matchingBooks: [],
            addedBooks,
        }));
        if (this.props.onChange) {
            this.props.onChange(addedBooks);
        }
    }
    handleSubmit = book => {
        let addedBooks = this.state.addedBooks;
        if (this.props.onChange) {
            this.props.onChange(addedBooks);
        }
    }
    handleRemove = book => {
        let addedBooks = this.state.addedBooks;
        addedBooks = addedBooks.filter(b => b.key !== book.key);
        this.setState({
            addedBooks,
        });
        if (this.props.onChange) {
            this.props.onChange(addedBooks);
        }
    }
    handleTypeChange = (book, e) => {
        let addedBooks = this.state.addedBooks;
        book.cardType = e.target.value;
        this.setState({ ...this.state });
        if (this.props.onChange) {
            this.props.onChange(addedBooks);
        }
    }
    render() {
        const {
            inputVal,
            matchingBooks,
            addedBooks,
        } = this.state;
        return <div className="select-books">
            <strong>{'Select Books '}</strong>
            <input value={inputVal} size="50" onChange={this.handleSearch} placeholder="search book title or author" />
            <ul className="">
                {matchingBooks.map(book =>
                    <li key={book.id}>
                        <Link onClick={() => this.handleAdd(book)}>> {book.title} by {bookUtil.author(book)}</Link>
                    </li>
                )}
            </ul>
            <br />
            <table>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>title</th>
                        <th>author</th>
                        <th>year</th>
                        <th>card type (select one)</th>
                        <th>actions</th>
                    </tr>
                </thead>
                <tbody>
                    {addedBooks.map(book => <tr key={book.key}>
                        <td>{book.id}</td>
                        <td>{book.title}</td>
                        <td>{bookUtil.author(book)}</td>
                        <td>{book.year}</td>
                        <td>
                            <select onChange={e => this.handleTypeChange(book, e)}>
                                <option>title</option>
                                <option>author</option>
                                <option>subject</option>
                            </select>
                        </td>
                        <td>
                            <a onClick={() => this.handleRemove(book)} href="#remove">[x]</a>
                        </td>
                    </tr>)}
                </tbody>
            </table>
            {/*<button onClick={this.handleSubmit}>submit</button>*/}
        </div>;
    }
}

class Link extends Component {
    render() {
        return <span className="clickable" onClick={this.props.onClick}>
            {this.props.text || this.props.children}
        </span>
    }
}

class AppHeader extends Component {
    render() {
        const {
            viewContent,
        } = this.props;
        return <div className="app-header">
            <div className="flex-row">
                <div>
                    <h1 className="sitename">Dilim</h1>
                    <p className="sitedesc">Digital Library Management</p>
                </div>
                <nav className="sitenav">
                <ul class="inline">
                    <li><Link text="browse" onClick={() => viewContent("browse")}/></li>
                    <li><Link text="card catalog" onClick={() => viewContent("card")}/></li>
                </ul>
                </nav>
            </div>
        </div>
    }
}

class App extends Component {
    cardGrid = React.createRef();
    state = {
        users: [],
        bookData: {
            id: 23,
            title: "some book title",
            classNumber: "100",
            authorFname: "ronald",
            authorLname: "casili",
            year: 2018,
            copyNumber: 2,

            isbn: "9783161484100",
            edition: "2",
            publisher: "shit books ltd.",
            location: "shithole",
            pagesFront: "xxiv",
            pagesContent: "238",
            size: "256x3cm",

            illustrations: 2,
        },
        cardConfig: {
            columns: 2,
            cardSize: 0,
            orientation: "landscape",
            format: "a4",
        },
        books: [],
        pageName: "browse",
    }
    componentDidMount() {
        //axios.fetch('/users')
        //    .then(users => this.setState({ users }));
    }
    handleSubmit = book => {
        this.setState({ bookData: book });
    }
    handlePrint1 = e => {
        const {cardConfig} = this.state;
        const cardGrid = this.cardGrid.current;
        const opt = {
            margin: 0,
            image: { type: 'jpeg', quality: 1 },
            jsPDF: {
                orientation: cardConfig.orientation.slice(0, 1),
                format: cardConfig.format,
            }
        }
        cardGrid.children[0].classList.add("print");
        html2pdf().from(cardGrid).set(opt).save()
            .then(() => {
                cardGrid.children[0].classList.remove("print");
            });
    }
    handlePrint2 = e => {
        window.print();
    }
    handleCardChange = cardConfig => {
        this.setState({ cardConfig });
    }
    handleSelectedBooksChange = books => {
        this.setState({ books });
    }
    handleBookUpdate = updatedBook => {
        const books = this.state.books.map(book => {
            if (updatedBook.id !== book.id) {
                return book;
            }
            return {
                ...book,
                ...updatedBook,
            }
        });
        this.setState({ books });
    }
    handlePage = pageName => {
        this.setState({ pageName })
    }
    render() {
        const {bookData} = this.state;
        const {
            cardConfig,
            pageName,
        } = this.state;
        let  pages = {
            browse: (
            <div className="no-print">
                <BrowseBooks onUpdate={this.handleBookUpdate}/>
            </div>
            ),
            card: (
            <div className="panel">
                <SelectBooks books={this.state.books} onChange={this.handleSelectedBooksChange}/>
                <div ref={this.cardGrid}>
                    <CardGrid cols={cardConfig.columns} rows={cardConfig.rows} format={cardConfig.format} orientation={cardConfig.orientation}>
                        {this.state.books.map(book => {
                            switch (book.cardType) {
                            case "subject": return <SubjectCard size={cardConfig.size} book={book} />;
                            case "author": return <AuthorCard size={cardConfig.size} book={book} />;
                            case "title":
                            default:
                                return <TitleCard size={cardConfig.size} book={book} />;
                            }
                    })}
                    </CardGrid>
                </div>
                <div className="print-buttons no-print flex-row">
                    <ConfigCard onChange={this.handleCardChange}/>
                    <button onClick={this.handlePrint1}>print option 1</button>
                    {/*<button onClick={this.handlePrint2}>print option 2</button>*/}
                </div>
            </div>
            ),
        }

        let content = pages[pageName] || pages["browse"];
        return (
            <div className="App">
                <AppHeader viewContent={this.handlePage} />
                {content}
            </div>
        );
    }
}

export default App;