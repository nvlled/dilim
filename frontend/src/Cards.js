
import {andJoin, commaJoin, surround, prefix, suffix} from "./util";
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import * as ddcsum from "ddcsum";
import * as ordinal from "ordinal";
import html2pdf from "html2pdf.js";
const axios = require("axios");

const addCm = n => {
    n = n+"";
    if (n.match(/cm$/i) || !n)
        return n;
    return `${n}cm`;
}
const breakLines = str => {
    str = (str||"").trim();
    if (!str)
        return "";
    return str.split("\n").map(s => [s, <br />]);
}

export class AuthorCard extends Component {
    render() {
        let {book} = this.props;
        let {
            id: accessionNum,
            authorFname: fname,
            authorLname: lname,
            pagesContent,
            pagesFront,
            illustrations,
            edition,
            hasIndex,
            hasBibliography,
            hasGlossary,
            partOfBook,
            remarks,
            otherInfo,
        } = book;
        const illustrationsDesc = illustrations;

        if (isNaN(+edition))
            edition = 1;

        return (
            <div className="card-catalog flex-row">
                <div className="flex-col pr50">
                    <br />
                    <div>{book.locator}</div>
                    <div>{(accessionNum+"").padStart(4, "0")}</div>
                    <div>{book.cutterNumber}</div>
                    <div>{book.year}</div>
                </div>
                <div style={{width: "100%"}}>
                    <br />
                    <br />
                    <br />
                    <div className="">{lname}, {fname}</div>
                    <div className="indent">{book.title} / {fname} {lname} -- {ordinal(+edition)} Edition. -- {book.location}. : {book.publisher}, c{book.year}</div>
                    <div className="indent">{commaJoin(pagesFront, suffix(pagesContent, "p."))} {prefix(" : ", illustrationsDesc)}{prefix(" ;  ", addCm(book.size))} </div>
                    <br />
                    <div className="indent">{surround("Includes: ", partOfBook, ".")}</div>
                    <div className="indent">ISBN: {book.isbn || "--"} {surround("(", remarks, ")")}</div>
                    <br />
                    <div className="indent">{(book.otherInfo||"").trim()}</div>
                </div>
            </div>
        )
    }
}

export class TitleCard extends Component {
    render() {
        let {book} = this.props;
        let {
            id: accessionNum,
            authorFname: fname,
            authorLname: lname,
            pagesContent,
            pagesFront,
            illustrations,
            edition,
            hasIndex,
            hasBibliography,
            hasGlossary,
            callNumber="",
            partOfBook,
            remarks,
        } = book;
        const illustrationsDesc = illustrations;
        callNumber = callNumber || "";

        if (isNaN(+edition))
            edition = 1;

        const includeList = [hasIndex && "index", hasBibliography && "bibliography", hasGlossary && "glossary"];
        const includes = andJoin(...includeList.filter(x => !!x));

        return (
            <div className="card-catalog flex-row">
                <div className="flex-col pr50">
                    <br />
                    <div>{book.locator}</div>
                    {callNumber.split(" ").map(f => <div>{f}</div>)}
                </div>
                <div style={{width: "100%"}}>
                    <br />
                    <br />
                    <br />
                    <div className="unindent">{book.title} / {book.publisher} -- {book.location} : {fname} {lname}, c{book.year}</div>
                    <div className="">{commaJoin(pagesFront, suffix(pagesContent, "p."))} {prefix(" : ", illustrationsDesc)} {prefix(" ; ", addCm(book.size))} </div>
                    <br />
                    <div className="">{surround("Includes: ", partOfBook, ".")}</div>
                    <div className="">ISBN: {book.isbn || "--"} {surround("(", remarks, ")")}</div>
                    <br />
                    <div className="">{breakLines(book.otherInfo)}</div>
                </div>
            </div>
        )
    }
}

export class SubjectCard extends Component {
    render() {
        let {book} = this.props;
        let {
            id: accessionNum,
            authorFname: fname,
            authorLname: lname,
            pagesContent,
            pagesFront,
            illustrations,
            edition,
            hasIndex,
            hasBibliography,
            hasGlossary,
            callNumber="",
            classHeading,
            partOfBook,
            remarks,
        } = book;
        callNumber = callNumber || "";
        const illustrationsDesc = illustrations;

        if (isNaN(+edition))
            edition = 1;

        if (!classHeading) {
            classHeading = ddcsum.data[book.classNumber];
        }

        return (
            <div className="card-catalog subject-card">
                <div className="flex-row space">
                    <div>{suffix(book.classHeading, " --")} {book.subject}</div>
                    <div>{book.locator}</div>
                </div>
                <div className="flex-row">
                    <div className="flex-col pr50">
                        {callNumber.split(" ").map(f => <div>{f}</div>)}
                    </div>
                    <div style={{width: "100%"}}>
                        <div className="">{lname}, {fname}</div>
                        <div className="indent">{book.title} / {fname} {lname} -- {ordinal(+edition)} Edition. -- {book.location}. : {book.publisher}, c{book.year}</div>
                        <div className="indent">{commaJoin(pagesFront, suffix(pagesContent, "p."))} {prefix(" : ", illustrationsDesc)} {prefix(" ; ", addCm(book.size))} </div>
                        <br />
                        <div className="indent">{surround("Includes: ", partOfBook, ".")}</div>
                        <div className="indent">ISBN: {book.isbn || "--"} {surround("(", remarks, ")")}</div>
                    </div>
                </div>
            </div>
        );
    }
}