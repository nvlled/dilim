
export const atMost = (millis, fn) => {
    let waiting = false;
    let savedArgs = [];
    return (...args) => {
        savedArgs = args;
        if (waiting) {
            return;
        }
        waiting = true;
        setTimeout(() => {
            fn(...savedArgs);
            waiting = false;
        }, millis);
    }
}

export const commaJoin = (...args) => args.filter(x => !!x).join(", ");
export const prefix = (pref, str) => !!str ? `${pref}${str}` : "";
export const suffix = (str, suf) => !!str ? `${str}${suf}` : "";

export const andJoin = (...args) => {
    if (args.length <= 1)
        return args.join("");
    const [a, b] = args.slice(args.length - 2);
    const result = args.slice(0, args.length-2).concat(`${a} and ${b}`).join(", ");
    return result;
}

export const surround = (...args) => {
    let pref, suf, str;
    if (args.length === 1)
        ([suf] = args);
    else if (args.length === 2)
        ([str, suf] = args);
    else
        ([pref, str, suf] = args);
    if (!str)
        return;
    return `${pref||''}${str||''}${suf||''}`;
}

export const bookUtil = {
    author(book) {
        return commaJoin(book.authorLname, book.authorFname);
    }
}

export const range = n => {
    let xs = [];
    for (let i = 0; i < n; i++) {
        xs.push(i);
    }
    return xs;
}